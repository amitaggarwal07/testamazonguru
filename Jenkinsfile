#!groovy
@Library('fc-shared-libraries') _
String VAR_LIB  = "fcsky-common-lib"
String branchName = env.BRANCH_NAME
def getShortCommitHash() {
    return sh(returnStdout: true, script: "git log -n 1 --pretty=format:'%h'").trim()   
}
String determineRepoName() {
    return scm.getUserRemoteConfigs()[0].getUrl().tokenize('/').last().split("\\.")[0]
}


pipeline {

     agent {
        docker {
            label 'CI-JENKINS-SLAVE'
            reuseNode false
            image '155086999298.dkr.ecr.us-east-1.amazonaws.com/cicd/centos_fc_v1:latest' 
            args  '-u root:root -v /var/run/docker.sock:/var/run/docker.sock:rw,z' 
      }
    }
    
    parameters {
      
      booleanParam(defaultValue: false, description: 'Deploy to RELEASE_QA Environment ?', name: 'DEPLOY_RELEASE_QA')
      booleanParam(defaultValue: false, description: 'Deploy to PRODUCTION_QA Environment ?', name: 'DEPLOY_PRODUCTION_QA')
      booleanParam(defaultValue: false, description: 'Deploy to RELEASE_UAT Environment ?', name: 'DEPLOY_RELEASE_UAT')
      booleanParam(defaultValue: false, description: 'Deploy to PROD Environment ?', name: 'DEPLOY_PROD')
      booleanParam(defaultValue: false, description: 'CS_SCAN ?', name: 'CS_SCAN')    
      }

triggers {
  cron ( env.BRANCH_NAME == 'master' ? '' : '' )
  parameterizedCron(env.BRANCH_NAME == 'master' ? '''
  # RELEASE_DEV DEPLOY  Scheduled at 10,13,16,19,22 IST
  # RELEASE_QA DEPLOY  Scheduled at 20,08 IST
  30 12 * * 1-5 %CS_SCAN=true
  00 20 * * 0-4 %DEPLOY_RELEASE_QA=true
  ''' : '')
}
environment {
        BRANCH_NAME = "${env.BRANCH_NAME}"
        APP_NAME = determineRepoName()
        BUILD_NUMBER = "${env.BUILD_NUMBER}"
        IMAGE_VERSION = "v_${BUILD_NUMBER}"
        COMMIT_ID = getShortCommitHash()
        RELEASE_DEV_ECR_REPO_URL = "155086999298.dkr.ecr.us-east-1.amazonaws.com/msa-dev"
        RELEASE_QA_ECR_REPO_URL = "155086999298.dkr.ecr.us-east-1.amazonaws.com/msa-qa"
      }
options {
        buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '31', numToKeepStr: '31'))
        timestamps()
        timeout time:120, unit:'MINUTES'
    }
    
stages {
        stage("Initialize") {
            steps {
                script {
                    echo "${BUILD_NUMBER} - ${env.BUILD_ID} on ${env.JENKINS_URL}"
                    echo "Branch Specifier :: ${params.SPECIFIER}"
                    echo "Deploy to RELEASE_QA? :: ${params.DEPLOY_RELEASE_QA}"
                    echo "Deploy to PRODUCTION_QA? :: ${params.DEPLOY_PRODUCTION_QA}"
                    echo "Deploy to RELEASE_UAT? :: ${params.DEPLOY_RELEASE_UAT}"
                    echo "Deploy to PROD? :: ${params.DEPLOY_PROD}"
                }
            }
        }   
stage('Build') {
        when {
            expression { params.DEPLOY_RELEASE_QA == false && params.DEPLOY_PRODUCTION_QA == false && params.DEPLOY_RELEASE_UAT == false && params.DEPLOY_PROD == false }
        }
        steps {
                   script {
                   
                       msabuild(WORKSPACE,VAR_LIB,APP_NAME,BRANCH_NAME)
                       
                          }
                    }
                  }
stage ('Unit Test') {
    when { expression { params.DEPLOY_RELEASE_QA == false && params.DEPLOY_PRODUCTION_QA == false && params.DEPLOY_RELEASE_UAT == false && params.DEPLOY_PROD == false } }
     
      steps {
            unittest(WORKSPACE,APP_NAME)
            junit 'build/test-results/test/*.xml'
            publishHTML target: [
            allowMissing: false,
            alwaysLinkToLastBuild: false,
            keepAll: true,
            reportDir: 'build/test-results/test/',
            reportFiles: '*.xml',
            reportName: 'junit-Report.xml'
          ] 
                        }
       
       }
stage('Parallel Stage 1') {
when { expression { params.DEPLOY_RELEASE_QA == false && params.DEPLOY_PRODUCTION_QA == false && params.DEPLOY_RELEASE_UAT == false && params.DEPLOY_PROD == false } }
            parallel {                

stage ('Code Quality Anaylsis') {
    when { expression { params.DEPLOY_RELEASE_QA == false && params.DEPLOY_PRODUCTION_QA == false && params.DEPLOY_RELEASE_UAT == false && params.DEPLOY_PROD == false } }
        steps {
         withSonarQubeEnv('sonarqube') {
         sonar(WORKSPACE,APP_NAME,BRANCH_NAME,BUILD_NUMBER)
                           }
        timeout(time: 60, unit: 'MINUTES') {
            waitForQualityGate abortPipeline: true
        }
   }
}
              
stage ('Integration/Component Testing') {
 when { expression { env.BRANCH_NAME == 'master' && params.DEPLOY_RELEASE_QA == false && params.DEPLOY_PRODUCTION_QA == false && params.DEPLOY_RELEASE_UAT == false && params.DEPLOY_PROD == false && params.DEPLOY_RELEASE_DEV == false || params.DEPLOY_RELEASE_DEV == true } }
steps {
                 
           echo "work in progress"
              /*    script {
                    msaIntegrationTest()
                    publishHTML target: [
                    allowMissing: false,
                    alwaysLinkToLastBuild: false,
                    keepAll: true,
                    reportDir: 'build/test-results/integrationTest/',
                    reportFiles: '*.xml',
                    reportName: 'integrationTest-Report'
          ]            
         }
            */        
    }
 }
stage ('Jar vulnerabilities') {
  when {
      expression { env.BRANCH_NAME == 'master' && params.CS_SCAN == true }
   }
steps {
         dependencyCheck()
            publishHTML target: [
            allowMissing: false,
            alwaysLinkToLastBuild: false,
            keepAll: true,
            reportDir: 'build/reports/',
            reportFiles: 'dependency-check-report.html',
            reportName: 'Dependency-Check-Report'
          ]
                  }
                }
   }
}         
                
stage('Container Image Creation') {
  when {
      expression { env.BRANCH_NAME == 'master' && params.DEPLOY_RELEASE_QA == false && params.DEPLOY_PRODUCTION_QA == false && params.DEPLOY_RELEASE_UAT == false && params.DEPLOY_PROD == false }

    }
    
            steps {
             sh 'pwd ;cd src/main/docker/;docker build -t ${APP_NAME}:${BRANCH_NAME}_${COMMIT_ID}_${BUILD_NUMBER} .'
            }
        }
stage('Publish Image on RELEASE_DEV ECR') {
  when {
      expression { env.BRANCH_NAME == 'master' && params.DEPLOY_RELEASE_QA == false && params.DEPLOY_PRODUCTION_QA == false && params.DEPLOY_RELEASE_UAT == false && params.DEPLOY_PROD == false }

    }
    
          
       steps {
       sh 'aws ecr get-login --no-include-email --region us-east-1 | sh;docker tag ${APP_NAME}:${BRANCH_NAME}_${COMMIT_ID}_${BUILD_NUMBER} ${RELEASE_DEV_ECR_REPO_URL}/${APP_NAME}:${BRANCH_NAME}_${COMMIT_ID}_${BUILD_NUMBER};docker push ${RELEASE_DEV_ECR_REPO_URL}/${APP_NAME}:${BRANCH_NAME}_${COMMIT_ID}_${BUILD_NUMBER}'
            }
        }
stage('Container Security Scan') {       
  when {
      expression { env.BRANCH_NAME == 'master' && params.CS_SCAN == true }
   }
     steps {
      script {
         def imageLine = "${RELEASE_DEV_ECR_REPO_URL}/${APP_NAME}:${BRANCH_NAME}_${COMMIT_ID}_${BUILD_NUMBER}"
         writeFile file: 'anchore_images', text: imageLine
         anchore bailOnFail: true, bailOnPluginFail: true, name: 'anchore_images'
               }
            }
        }       
         
stage('Deploy - RELEASE_DEV') {
  when {
      expression { env.BRANCH_NAME == 'master' && params.DEPLOY_RELEASE_QA == false && params.DEPLOY_PRODUCTION_QA == false && params.DEPLOY_RELEASE_UAT == false && params.DEPLOY_PROD == false }

    }
            steps {

                   echo "Deploying to RELEASE_DEV Environment."
                sh 'ENV_NAME="dev";CLUSTER_NAME="ECS-DEV";sh src/main/docker/Deployment.sh ${APP_NAME} ${ENV_NAME} ${BRANCH_NAME} ${COMMIT_ID} ${BUILD_NUMBER} ${CLUSTER_NAME} ${RELEASE_DEV_ECR_REPO_URL}'
            }
        }

stage('Parallel Stage 2') {
  when {
      expression { env.BRANCH_NAME == 'master' && params.DEPLOY_RELEASE_QA == false && params.DEPLOY_PRODUCTION_QA == false && params.DEPLOY_RELEASE_UAT == false && params.DEPLOY_PROD == false }

    }
            parallel {

stage ('API TEST') {
  when {
      expression { env.BRANCH_NAME == 'master' && params.DEPLOY_RELEASE_QA == false && params.DEPLOY_PRODUCTION_QA == false && params.DEPLOY_RELEASE_UAT == false && params.DEPLOY_PROD == false }

    }

steps {  
           karate(WORKSPACE,APP_NAME)
      }
    }
    
stage ('Performance Test') {
  when {
      expression { env.BRANCH_NAME == 'master' && params.DEPLOY_RELEASE_QA == false && params.DEPLOY_PRODUCTION_QA == false && params.DEPLOY_RELEASE_UAT == false && params.DEPLOY_PROD == false }

    }
      steps {
               performance(WORKSPACE,APP_NAME)
        }
      }   
         
 stage('BAT') {
  when {
      expression { env.BRANCH_NAME == 'master' && params.DEPLOY_RELEASE_QA == false && params.DEPLOY_PRODUCTION_QA == false && params.DEPLOY_RELEASE_UAT == false && params.DEPLOY_PROD == false }

    }
            steps {
                echo "BAT work in progress"
                  }
        }
        
}
 }

stage('Publish Same Image on RELEASE_QA ECR') {
  when {
      expression { env.BRANCH_NAME == 'master' && params.DEPLOY_RELEASE_QA == false && params.DEPLOY_PRODUCTION_QA == false && params.DEPLOY_RELEASE_UAT == false && params.DEPLOY_PROD == false }

    }
          
       steps {
             echo "publish to RELEASE_QA ECR..."
             sh 'aws ecr get-login --no-include-email --region us-east-1 | sh;docker tag ${APP_NAME}:${BRANCH_NAME}_${COMMIT_ID}_${BUILD_NUMBER} ${RELEASE_QA_ECR_REPO_URL}/${APP_NAME}:${BRANCH_NAME}_${COMMIT_ID}_${BUILD_NUMBER};docker push ${RELEASE_QA_ECR_REPO_URL}/${APP_NAME}:${BRANCH_NAME}_${COMMIT_ID}_${BUILD_NUMBER}; docker rmi ${APP_NAME}:${BRANCH_NAME}_${COMMIT_ID}_${BUILD_NUMBER}; docker rmi ${RELEASE_QA_ECR_REPO_URL}/${APP_NAME}:${BRANCH_NAME}_${COMMIT_ID}_${BUILD_NUMBER};docker rmi ${RELEASE_DEV_ECR_REPO_URL}/${APP_NAME}:${BRANCH_NAME}_${COMMIT_ID}_${BUILD_NUMBER}'
                 }
        }
stage('Deploy - RELEASE_QA') {
  when {
      expression { env.BRANCH_NAME == 'master' && params.DEPLOY_RELEASE_QA == true }
   }
          steps {
                   echo "Deploy to RELEASE_QA..."
                   sh 'ENV_NAME="qa";CLUSTER_NAME="ECS-QA";aws ecr get-login --no-include-email --region us-east-1;IMAGE_TAG=`aws ecr describe-images --output json --repository-name  msa-${ENV_NAME}/${APP_NAME} --query "sort_by(imageDetails,& imagePushedAt)[-1].imageTags[0]" | jq . --raw-output`;BRANCH_NAME=`echo $IMAGE_TAG | cut -d "_" -f1`;COMMIT_ID=`echo $IMAGE_TAG | cut -d "_" -f2`;BUILD_NUMBER=`echo $IMAGE_TAG | cut -d "_" -f3`;sh src/main/docker/Deployment.sh ${APP_NAME} ${ENV_NAME} ${BRANCH_NAME} ${COMMIT_ID} ${BUILD_NUMBER} ${CLUSTER_NAME} ${RELEASE_QA_ECR_REPO_URL}'
          }
         }
         
stage('Parallel Stage 3') {
  when {
      expression { env.BRANCH_NAME == 'master' && params.DEPLOY_RELEASE_QA == true }
   }
            parallel {         
stage('E2E Testing') {
  when {
      expression { env.BRANCH_NAME == 'master' && params.DEPLOY_RELEASE_QA == true }
   }
    steps {
script {
        try {
         e2e(WORKSPACE,APP_NAME)  
        }catch(Exception e){
            print("Need To Be Implemented failure strategies")
        }finally {     
      publishHTML target: 
      [
      allowMissing: false,
      alwaysLinkToLastBuild: false,
      keepAll: true,
      reportDir: 'E2E/FCMSAautomation/',
      reportFiles: '/*.html',
      reportName: 'E2E-Test-Report'
      ]  
          }
        }
       }
   }

stage('Component Testing') {
  when {
      expression { env.BRANCH_NAME == 'master' && params.DEPLOY_RELEASE_QA == true }
   }
          steps {
                   echo "RELEASE_QA Component Testing..."
               }
            }
}
}

stage('Deploy - PRODUCTION_QA') {
  when {
      expression { env.BRANCH_NAME == 'master' && params.DEPLOY_PRODUCTION_QA == true }
   }
            steps {
                echo "Deploy to PRODUCTION_QA..."
            }
        }
stage('Deploy - RELEASE_UAT') {
  when {
      expression { env.BRANCH_NAME == 'master' && params.DEPLOY_RELEASE_UAT == true }
   }
            steps {
                echo "Deploy to RELEASE_UAT......."
            }
        }
stage('Deploy - Production') {
  when {
      expression { env.BRANCH_NAME == 'master' && params.DEPLOY_PROD == true }
   }
            steps {
                echo "Deploy to PROD..."
            }
        }
    }

    post {
       always {
            echo "I AM ALWAYS first"
            //sh 'chown -Rv 509:495 build &>/dev/null'
            sendNotifications("${currentBuild.currentResult}","${COMMIT_ID}")
        }
        aborted {
            echo "BUILD ABORTED"
           // sh 'chown -Rv 509:495 build &>/dev/null'
        }
        success {
            echo "BUILD SUCCESS"
            //sh 'chown -Rv 509:495 build &>/dev/null'
        }
        unstable {
            echo "BUILD UNSTABLE"
            //sh 'chown -Rv 509:495 build &>/dev/null'
        }
        failure {
            echo "BUILD FAILURE"
            msaFailedNotifications("${currentBuild.currentResult}","${COMMIT_ID}","${APP_NAME}")
            //sh 'chown -Rv 509:495 build &>/dev/null'
        }
    }
}


