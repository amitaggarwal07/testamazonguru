#Overview

This application provides the **audit** related functionality and serves as one component. It defines the REST endpoints that are used to provide audit functionality.

This micro-service also provides an example of to call another OAuth2 protected service from within this service using OAuth2 client configuration. The OAuth2 bearer token that has been passed to the audit service is propagated to the "tasks" and "tenant-config" service to get the tasks and tenant configs for the given audit.

##Pre-requisites

### Projects that need to be started before
* [fcsky-config server](/../fcsky-config-server/README.md) - For pulling the configuration information
* [fcsky-service-registry](/../fcsky-service-registry/README.md) - For starting the Eureka server since the authorization server also is a micro-service that needs to be registered with Eureka server.


## Compile
You can compile `fcsky-audit-service` alone as well as in group with other components and services. 
### 1. Compile with `fcsky-boot`
This will compile all the micro-services components and services as well.
```bash
cd /home/fran/FCSKY-BASE/fcsky-boot (Boot project directory)
sh build-all-projects.sh
```

Note : The compilation with boot is very fast since it compiles all the projects in parallel.
### 2. Compile `fcsky-audit-service` Only
```bash
cd /home/fran/FCSKY-BASE/fcsky-audit-service (fcsky-audit-service project directory)
./gradlew clean build;
```
## Run 
`fcsky-audit-service` will be deployed and run on embedded tomcat. A jar will be deployed for this which contains application server as well. 
### 1. Run with `fcsky-boot`
This will run all the micro-services components and services as well.
```bash
cd /home/fran/FCSKY-BASE/fcsky-boot (Boot project directory)
sh run-all-projects.sh
```
### 2. Run `fcsky-audit-service` Only
```bash
cd /home/fran/FCSKY-BASE/fcsky-audit-service/fcsky-audit-service/build/libs (fcsky-audit-service project directory)
java -jar fcsky-audit-service-0.0.1.jar &
```

## External Configuration
Please refer to [user webservice](/../../blob/master/user-webservice/README.md) for details on how the external configuration works. Note that there is separate configuration file for each Spring application; the application should refer to it's own .yml file for configuration.

## Consumer Driven Contracts
* The audit service has tests that validate the contract defined by the comments webservice.
* The `comments-webservice` publishes the stubs to the local maven repository which is then used by the `audit-webservice` to validate the contract.
